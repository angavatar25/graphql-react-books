const express = require('express');
const { graphqlHTTP } = require('express-graphql');
const schema = require('./schema/schema');
const moongose = require('mongoose');

const app = express();

moongose.connect(
    'mongodb+srv://angavatar:Aventador12@graphql-react.l6swy.mongodb.net/author?retryWrites=true&w=majority');
moongose.connection.once('open', () => {
    console.log('Connected to database');
})

app.use('/graphql', graphqlHTTP({
    schema,
    graphiql: true
}))

app.listen(4000, () => {
    console.log('Listening at port 4000');
});